package com.syberry.test.service;

import com.syberry.test.exception.CyclicDependencyException;
import com.syberry.test.exception.EntityNotFoundException;
import com.syberry.test.model.Task;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TaskScheduler implements IScheduler {

    private static final String FAILED_TO_FIND_TASK = "Failed to find task with name - \"{0}\" in task list.";
    private static final String CYCLIC_DEPENDENCY_DETECTED = "Cyclic dependency detected. Task \"{0}\" and task" +
            " \"{1}\" have cyclic dependency.";

    @Override
    public List<Task> schedule(List<Task> tasks) {
        List<Task> sortedTasks = new LinkedList<>();
        tasks.forEach(task -> {
            processTasksRecursively(tasks, sortedTasks, task);
            checkTaskStatus(sortedTasks, task);
        });
        return sortedTasks;
    }

    private void processTasksRecursively(List<Task> tasks, List<Task> sortedTasks, Task task) {
        if (isTaskHasAnyPredecessors(task)) {
            List<String> predecessors = task.getPredecessors();
            predecessors.forEach(predecessor -> {
                Task foundTask = foundTaskByPredecessorName(tasks, predecessor);
                checkCyclicDependency(task, foundTask);
                processTasksRecursively(tasks, sortedTasks, foundTask);
                checkTaskStatus(sortedTasks, foundTask);
            });
        }
    }

    private Task foundTaskByPredecessorName(List<Task> tasks, String predecessor) {
        Optional<Task> optionalTask = tasks
                .stream()
                .filter(taskToFind -> predecessor.equals(taskToFind.getName()))
                .findAny();
        if (!optionalTask.isPresent()) {
            String message = MessageFormat.format(FAILED_TO_FIND_TASK, predecessor);
            throw new EntityNotFoundException(message);
        }
        return optionalTask.get();
    }

    private void checkTaskStatus(List<Task> sortedTasks, Task foundTask) {
        if (!sortedTasks.contains(foundTask)) {
            sortedTasks.add(foundTask);
        }
    }

    private void checkCyclicDependency(Task task, Task foundTask) {
        boolean isCyclic = foundTask.getPredecessors()
                .stream()
                .anyMatch(foundPredecessor -> foundPredecessor.equals(task.getName()));
        if (isCyclic) {
            String message = MessageFormat.format(CYCLIC_DEPENDENCY_DETECTED, task.getName(), foundTask.getName());
            throw new CyclicDependencyException(message);
        }
    }

    private boolean isTaskHasAnyPredecessors(Task task) {
        List<String> predecessors = task.getPredecessors();
        return !predecessors.isEmpty();
    }
}
