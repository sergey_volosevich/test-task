package com.syberry.test.controller;

import com.syberry.test.model.Task;
import com.syberry.test.service.IScheduler;
import com.syberry.test.service.TaskScheduler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Task> tasks = Arrays.asList(
                new Task("E", Collections.singletonList("B")),
                new Task("D", Arrays.asList("A", "B")),
                new Task("A", Collections.emptyList()),
                new Task("B", Collections.singletonList("A")),
                new Task("C", Arrays.asList("D", "B")),
                new Task("F", Collections.singletonList("E"))
        );

        IScheduler scheduler = new TaskScheduler();
        List<Task> sortedTasks = scheduler.schedule(tasks);
        for (Task t : sortedTasks) {
            System.out.println(t.getName());
        }
    }
}
