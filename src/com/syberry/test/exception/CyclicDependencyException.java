package com.syberry.test.exception;

public class CyclicDependencyException extends RuntimeException {

    private static final long serialVersionUID = 4010062389151660523L;

    public CyclicDependencyException(String message) {
        super(message);
    }
}
