package com.syberry.test.exception;

public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -6999106045975694423L;

    public EntityNotFoundException(String message) {
        super(message);
    }
}
